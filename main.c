#include <stdio.h> //io for the shell, fgets
#include <unistd.h> //fork(), system call
#include <sys/wait.h> //wait(), system call
#include <string.h> //for string comparison
#define MAX_LINE 80 // The maximum length command  - constant*/
#define MAX_ARGS 41 //maximum length of the params


// functions prototypes
void execute(char **allCommandTokens, int isOnBackground);

int parser(char *allCommandString, char **spaceToSaveTokens);

int main(void) {
    //pointer array (char *args[MAX_ARGS]) -> every pointer points to a char * (string pointer) (pointer of pointers)
    char *args[MAX_ARGS]; /* command line arguments */
    char fullCmd[MAX_LINE];
    int should_run = 1; /* flag to determine when to exit program */

    //start of the prompt
    while (should_run) {
        printf("unsh>> ");
        //read the input in safe way with fgets
        fgets(fullCmd, MAX_LINE, stdin);
        int isOnBackground = parser(fullCmd, args);

        if (strcmp(args[0], "exit") == 0){
            should_run = 0;
        }
        else{
            execute(args, isOnBackground);
        }
        //printf("%s", *(args + 3 - 1));
    }
}

// * After reading user input, the steps are:
int parser(char *allCommandString, char **spaceToSaveTokens) {
    //here *allCommandString --> dereferenced pointer, so we have the character
    while (*allCommandString != '\0') {
        while (*allCommandString == ' ' || *allCommandString == '\n'){
            //here, we put a \0 or NULL value, then we go a position more in the string
            //and then we evaluate again, this because a commando does not accept a space or a \n
            /*
             * The \0 character does not mark the "end of the array". The \0 character marks the end
             * of the string stored in a char array, if (and only if) that char array is intended to
             * store a string.
             */
            *allCommandString++ = '\0';
        }
        //save the position where a param starts, so ls, if l is index 0, here is the address of l
        //until s
        *spaceToSaveTokens++ = allCommandString;
        //printf("%s", line);
        while (*allCommandString != '\0' && *allCommandString != ' ' && *allCommandString != '\n'){
            //here we jump the positions until we find a \0, so we found another param
            allCommandString++;
        }

    }
    //we put the last and the second last in null; the second last is necessary for the way of fgets takes
    //the input and also NULL is necessary for execvp
    *spaceToSaveTokens = NULL;
    //here we take the actual address of pointer in the array and the second last is NULL too
    *(spaceToSaveTokens - 1) = NULL;

    if(strcmp(*(spaceToSaveTokens - 2),"&") == 0){
        return 1;
    }
    return 0;
}


void execute(char **allCommandTokens, int isOnBackground) {
    /**
* (1) fork a child process using fork()
* (2) the child process will invoke execvp()
* (3) parent will invoke wait() unless command included &
*/
    //system data type declared in unistd.h
    pid_t pid;
    //we use it to wait system call
    int status;
    /* fork a child process */
    pid = fork();
    if (pid < 0) { /* error occurred */
        fprintf(stderr, "Fork Failed :\\, if the error persists, close the terminal \n");
        exit(1);
    }
    else if (pid == 0) { /* child process */
        int status_children = execvp(*allCommandTokens, allCommandTokens);
        if(status_children == -1){
            printf("An error ocurred when the command was executed ... \n");
            exit(1);
        }
    }
    else { /* parent process */
    /* parent will wait for the child to complete */
        if(isOnBackground){
            printf("Child in background :) \n");
        }
        else{
            while(wait(&status) != pid){
                puts("waiting ...");
            }
        }

    }
}


/*how to know when the first
        int firstFinalIndex = 0;
        for (int i = 0; i <= strlen(fullCmd); ++i) {
            if (fullCmd[i] == ' ' || fullCmd[i] == '\0') {
                firstFinalIndex = i + 1;
                i = (int) strlen(fullCmd) + 1;
            }
            char *tempPointer = &fullCmd[i];
            cmd[i] = tempPointer;
        }
        int next_chr = 0;

        for (int i = firstFinalIndex; i <= strlen(fullCmd); ++i) {
            char *tempPointer = &fullCmd[i];
            args[next_chr] = tempPointer;
            next_chr++;
}*/